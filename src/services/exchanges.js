import _ from 'lodash';
import axios from 'axios';

const availableExchanges = {
  coinbase: 'https://api.coinbase.com/v2',
  kraken: 'https://api.kraken.com/0/public',
};

const coinbase = {
  currencies: async () => {
    const currencies = await axios.get(`${availableExchanges.coinbase}/currencies`);
    return _.map(_.get(currencies, 'data.data', []), curr => ({ text: curr.id, value: curr.name }));
  },
  exchangeRates: async (baseCurrency, pairCurrency) => {
    const exchanges = await axios.get(`${availableExchanges.coinbase}/exchange-rates?currency=${baseCurrency}`);
    return _.get(exchanges, ['data', 'data', 'rates', pairCurrency], 'Currency-Pair Rate Not Found');
  },
};

// const kraken = {
//   currencies: async () => {
//     const currencies = await axios.get(`${availableExchanges.kraken}/AssetPairs`);
//     return currencies;
//   },
//   // exchangeRates: async (baseCurrency, pairCurrency) => {

//   // }
// };

// const allExchanges = [
//   'BTC38', 'BTCC', 'BTCE', 'BTER', 'Bit2C', 'Bitfinex',
//   'Bitstamp', 'Bittrex', 'CCEDK', 'Cexio', 'Coinbase',
//   'Coinfloor', 'Coinse', 'Coinsetter', 'Cryptopia', 'Cryptsy',
//   'Gatecoin', 'Gemini', 'HitBTC', 'Huobi', 'itBit', 'Kraken',
//   'LakeBTC', 'LocalBitcoins', 'MonetaGo', 'OKCoin', 'Poloniex',
//   'Yacuna', 'Yunbi', 'Yobit', 'Korbit', 'BitBay', 'BTCMarkets',
//   'QuadrigaCX', 'CoinCheck', 'BitSquare', 'Vaultoro', 'MercadoBitcoin',
//   'Unocoin', 'Bitso', 'BTCXIndia', 'Paymium', 'TheRockTrading',
//   'bitFlyer', 'Quoine', 'Luno', 'EtherDelta', 'Liqui', 'bitFlyerFX',
//   'BitMarket', 'LiveCoin', 'Coinone', 'Tidex', 'Bleutrade', 'EthexIndia',
// ];

const allExchanges = ['Coinbase', 'Kraken'];

// const allCurrencies = async () => {
//   const coins = await axios.get('/static/coins.json');
//   return _.map(_.get(coins, 'data.Data', []),
// (value, key) => ({ text: key, value: value.CoinName }));
// };

const allCurrencies = async () => {
  const coins = await axios.get('https://api.gdax.com/currencies');
  return _.map(_.get(coins, 'data', []), value => ({ text: value.name, value: value.id }));
};

const exchangeRates = async (from, to, exchange) => {
  const exchanges = await axios.get(`https://min-api.cryptocompare.com/data/price?fsym=${from}&tsyms=${to}&e=${exchange}`);
  return _.get(exchanges, 'data', []);
};

export default {
  allExchanges,
  allCurrencies,
  availableExchanges,
  coinbase,
  exchangeRates,
};
